<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::model('user', 'App\User');
Route::model('post', 'App\Action');

Route::bind('meal', function ($value) {
    return App\Meal::where('id', $value)->where('user_id', auth()->user()->id)->where('editable', 1)->firstOrFail();
});

Route::bind('target', function ($value) {
    return App\Target::where('id', $value)->where('user_id', auth()->user()->id)->firstOrFail();
});

Route::bind('training', function ($value) {
    return App\Training::where('id', $value)->where('editable', 1)->where('user_id', auth()->user()->id)->firstOrFail();
});

Route::group(['middleware' => ['auth', 'csrf', 'bindings', 'adminactions'], 'prefix' => 'profile'], function(){

    Route::get('/', 'ProfileController@index')->name('profile');
    Route::post('update', 'ProfileController@update')->name('profile.update');
    Route::get('settings', 'ProfileController@settings')->name('profile.settings');
    Route::resource('meals', 'MealsController', ['only' => ['index', 'create', 'store']]);
    Route::resource('posts', 'PostsController', ['except' => ['show', 'edit', 'update']]);
    Route::resource('trainings', 'TrainingsController', ['except' => ['show']]);
    Route::resource('targets', 'TargetsController', ['except' => ['show']]);
    Route::post('targets/reorder', 'TargetsController@reorder')->name('targets.reorder');
    Route::get('targets/{target}/toggle', 'TargetsController@toggle')->name('targets.toggle');
    Route::get('messages', 'MessagesController@index')->name('messages.index');
    Route::get('messages/{user}', 'MessagesController@conversation')->name('messages.conversation');
    Route::post('messages/{user}', 'MessagesController@send')->name('messages.send');
    
});

Route::group(['middleware' => ['bindings']], function(){
    
    Route::get('/user/{user}', 'IndexController@user')->name('index.user');
    Route::get('/', 'IndexController@index')->name('index');

});

Route::get('avatar/{avatar}', function($avatar){
    return response()->file(storage_path('app/public/avatars/'.$avatar));
})->where(['avatar' => '.*'])->name('avatar');

Route::get('wallpaper/{wallpaper}', function($wallpaper){
    return response()->file(storage_path('app/public/wallpaper/'.$wallpaper));
})->where(['wallpaper' => '.*'])->name('wallpaper');

Route::get('photo/{photo}', function($photo){
    return response()->file(storage_path('app/public/photos/'.$photo));
})->where(['photo' => '.*'])->name('photo');

Auth::routes();
