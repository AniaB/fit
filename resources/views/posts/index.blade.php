@extends('layouts.dashboard')

@section('view')
    <h2>Posty</h2>
    <div class="row">
        <div class="col-xs-12 text-right">
            <a href="{{ route('posts.create') }}" class="btn btn-primary">Dodaj</a>
        </div>
    </div>
    <table class="table table-sortable">
        <thead>
            <tr>
                <th></th>
                <th></th>
                <th></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach($items as $item): ?>
                <tr data-id="{{ $item->id }}">
                    <td>
                        {{ $item->created_at->format('Y-m-d H:i:s') }}
                    </td>
                    <td>
                        <div class="post-lead">
                            {{ $item->lead }}
                            <?php if($item->automat): ?>
                                <span class="label label-default">automat</span>
                            <?php endif;?>
                        </div>
                    </td>
                    <td class="text-right">
                        <form action="{{ route('posts.destroy', ['post' => $item]) }}" method="POST">
                            {{ method_field('DELETE') }}
                            {{ csrf_field() }}
                            <a href="#" class="btn btn-sm toggle-bottom-row">
                                <span class="glyphicon glyphicon-chevron-up glyphicon-chevron-down"></span>
                            </a>
                            @include('partials.forms.submit', ['text' => 'usuń', 'class' => 'btn-sm btn-danger'])
                        </form>
                    </td>
                </tr>
                <tr class="hidden warning" data-id="{{ $item->id }}">
                    <td></td>
                    <td colspan="2">
                        {{ $item->content }}
                        <div class="row">
                            <?php foreach($item->photos as $photo): ?>
                                <div class="col-md-3 col-sm-4 col-xs-6">
                                    <img src="{{ $photo->url }}" alt="{{ $photo->filename }}" class="img-responsive img-rounded">
                                </div>
                            <?php endforeach; ?>
                        </div>
                    </td>
                </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
@endsection
