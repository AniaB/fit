{{ csrf_field() }}

<div id="create-post">
    <div class="row">
        <div class="col-xs-12">
            @include('partials.forms.textarea', ['item' => @$item, 'label' => 'Treść', 'option' => 'content', 'class' => "post-content"])
        </div>
        <div class="col-xs-12">
            @include('partials.forms.text', ['type' => 'file', 'item' => NULL, 'label' => 'Zdjęcia', 'option' => 'files[]', 'params' => ['multiple' => 'numtiple']])
        </div>
    </div>
</div>