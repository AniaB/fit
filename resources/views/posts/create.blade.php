@extends('layouts.dashboard')

@section('view')
    <h2>Nowy post</h2>
    <form action="{{ route('posts.store') }}" method="POST" enctype="multipart/form-data">
        @include('posts.form', ['item' => NULL])
        <div class="row">
            <div class="col-xs-12 text-right" >
                @include('partials.forms.submit', ['text' => 'Zapisz'])
            </div>
        </div>
        
    </form>
@endsection
