<div class="form-group<?= $errors->has($option) ?  ' has-error' : '' ?>">
    <label class="control-label">{{ $label }}</label> 
    <textarea name="{{ $option }}" placeholder="{{ $label }}" class="form-control {{ $class or '' }}" <?php if(@$params): ?><?php foreach(@$params as $param => $value): ?> {{ $param }}="{{ $value }}"<?php endforeach; ?><?php endif; ?>>{{ old(isset($property) ? $property : $option, isset($property) ? $property : @$item->$option) }}</textarea>
</div>