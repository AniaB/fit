<div class="form-group<?= $errors->has($option) ?  ' has-error' : '' ?>">
    <?php if(isset($label)): ?><label class="control-label">{{ $label }}</label><?php endif; ?> 
    {!! Form::select($option,
        $source,
        isset($property) ? $property : @$item->$option,
        [ 'class' => 'form-control m-b' ]
    ) !!}
</div>