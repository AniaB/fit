<div class="form-group<?= $errors->has($option) ?  ' has-error' : '' ?>">
    <?php if(isset($label)): ?><label class="control-label">{{ $label }}</label><?php endif; ?> 
    <div class="input-group date" id="datepicker">
        <input type="<?= isset($type) ? $type : 'text'; ?>" value="{{ old(isset($property) ? $property : $option, isset($property) ? $property : @$item->$option) }}" name="{{ $option }}" placeholder="{{ @$label }}" class="form-control {{ isset($class) ? $class  : '' }}" <?php if(@$params): ?><?php foreach(@$params as $param => $value): ?> {{ $param }}="{{ $value }}"<?php endforeach; ?><?php endif; ?>>
        <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
        <?php if(isset($help)): ?>
            <p class="help-block">{{ $help }}</p>
        <?php endif; ?>
    </div>
</div>