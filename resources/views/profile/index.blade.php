@extends('layouts.dashboard')

@section('view')
    <div class="row">
        <div class="col-xs-12">
            
            <div class="user-wallpaper" style="background-image:url('{{ $user->wallpaper }}')"></div>
            <div class="panel panel-default">
                <div class="panel-heading">Informacje</div>
                <div class="panel-body">
                    <?php if($user->current_target && $user->current_target->daily_info): ?>
                        {{ $user->current_target->daily_info }}
                    <?php else: ?>
                        <p>brak informacji</p><?php /*echo $user*/ ?> 
                    <?php endif; ?>
                </div>
            </div>
			
            <div class="panel panel-default">
                <div class="panel-heading">
                    <strong>Aktywny cel:</strong>
                    {{ $user->current_target ? $user->current_target->name : '' }}
                </div>
                <div class="panel-body">
                    <?php if($user->current_target): ?>
                        <p>
                            {{ $user->current_target->description }}
                        </p>
                        <div class="table-responsive">
                            <table class="table table-bordered">
                                <tbody>
                                    <tr>
                                        <td>Czas trwania (dni)</td>
                                        <td>{{ $user->current_target->duration }}</td>
                                    </tr>
                                    <tr>
                                        <td>Średnia ilość kalorii do spalenia dziennie</td>
                                        <td>{{ $user->current_target->daily_burnout }} kcal </td>
                                    </tr>
                                    <tr>
                                        <td>Docelowa waga</td>
                                        <td>{{ $user->current_target->weight }} kg</td>
                                    </tr>
                                    <tr>
                                        <td>Planujesz schudnąć</td>
                                        <td>{{ $user->current_target->difference }} kg</td>
                                    </tr>
                                </tbody>
                            </table>
                            
                        </div>
                    <?php else: ?>
                        Nie wybrałes jeszcze celu. Ustaw go w sekcji "cele"
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
@endsection
