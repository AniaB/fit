<div class="panel panel-default panel-user">
    <div class="row">
        <div class="col-xs-12 text-right">
            <a href="{{ route('messages.conversation', ['user' => $user]) }}" class="btn {{ (auth()->user() && auth()->user()->id !== $user->id) ? '' : 'disabled' }}">
                <i class="glyphicon glyphicon-envelope"></i>
            </a>
        </div>
        <div class="col-md-12 col-xs-12" align="center">
            <div class="user-wallpaper" style="background-image:url('{{ $user->wallpaper }}')">
                <a href="{{ route('index.user', ['user' => $user]) }}"class="outter user-avatar" style="background-image:url('{{ $user->avatar }}')"></a>
            </div>
            <h1>
                <a href="{{ route('index.user', ['user' => $user]) }}">
                    {{ $user->anonymus_name }}
                </a>
            </h1>
            <span>{{ $user->current_target ? $user->current_target->name : '&nbsp;' }}</span>
        </div>
        <div class="col-md-6 col-xs-6 follow line" align="center">
            <h3>
                {{ $user->height }} {{ $user->hunit }}
            </h3>
        </div>
        <div class="col-md-6 col-xs-6 follow line" align="center">
            <h3>
                wiek: {{ $user->age }}
            </h3>
        </div>
    </div>
</div>