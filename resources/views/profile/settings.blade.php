@extends('layouts.dashboard')

@section('view')
    <div class="row">
        <div class="col-xs-12">
            <form action="{{ route('profile.update') }}" method="POST" enctype="multipart/form-data">
                {{ csrf_field() }}
                <div class="panel panel-default">
                    <div class="panel-heading">Wizytówka</div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-sm-4 col-xs-12">
                                @include('partials.forms.text', ['item' => $user, 'label' => 'Imię', 'option' => 'name'])
                            </div>
                            <div class="col-sm-4 col-xs-12">
                                @include('partials.forms.text', ['item' => $user, 'label' => 'Nazwisko', 'option' => 'lastname'])
                            </div>
							<div class="col-sm-4 col-xs-12">
                                @include('partials.forms.text', ['item' => $user, 'label' => 'Login', 'option' => 'login'])
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-3 col-sm-6 col-xs-12">
                                @include('partials.forms.text', ['item' => $user, 'label' => 'Waga', 'option' => 'weight'])
                            </div>
                            <div class="col-md-3 col-sm-6 col-xs-12">
                                @include('partials.forms.text', ['item' => $user, 'label' => 'Wzrost', 'option' => 'height'])
                            </div>
                            <div class="col-md-3 col-sm-6 col-xs-12">
                                @include('partials.forms.text', ['item' => $user, 'label' => 'Rok urodzenia', 'option' => 'year'])
                            </div>
                            <div class="col-md-3 col-sm-6 col-xs-12">
                                @include('partials.forms.select', ['item' => $user, 'label' => 'Płeć', 'option' => 'gender', 'source' => \App\User::genders()])
                            </div>
                        </div>
                    </div>
                </div>

                <div class="panel panel-default">
                    <div class="panel-heading">Hasło</div>

                    <div class="panel-body">
                        <div class="row">
                            <div class="col-sm-6 col-xs-12">
                                @include('partials.forms.text', ['item' => $user, 'label' => 'Hasło', 'option' => 'password', 'type' => 'password', 'property' => ''])
                            </div>
                            <div class="col-sm-6 col-xs-12">
                                @include('partials.forms.text', ['item' => $user, 'label' => 'Powtórz hasło', 'option' => 'password2', 'type' => 'password', 'property' => ''])
                            </div>
                        </div>
                    </div>
                </div>

                <div class="panel panel-default">
                    <div class="panel-heading">Grafiki</div>

                    <div class="panel-body">
                        @include('partials.forms.text', ['item' => $user, 'label' => 'Plik awatara', 'option' => 'avatar', 'type' => 'file', 'property' => '', 'help' => 'wybierz plik PNG lub JPG'])
                        @include('partials.forms.text', ['item' => $user, 'label' => 'Plik tła', 'option' => 'wallpaper', 'type' => 'file', 'property' => '', 'help' => 'wybierz plik PNG lub JPG'])
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12 text-right">
                        <button type="submit" class="btn btn-primary">Zapisz</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection
