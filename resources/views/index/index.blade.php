@extends('layouts.app')
@section('content')
<div class="container">
    <div class="row">
        <?php foreach($users as $user): ?>
            <div class="col-md-3 col-sm-6 col-xs-12">
                @include('profile.card', ['user' => $user])
            </div>
        <?php endforeach; ?>
    </div>
</div>
@endsection
