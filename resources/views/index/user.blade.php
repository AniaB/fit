@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row user-wallpaper" style="background-image:url('{{ $user->wallpaper }}')">
        <div class="col-md-4 col-sm-6 col-xs-12">
            @include('profile.card', ['user' => $user])
        </div>
    </div>
    <div class="panel panel-primary">
        <div class="panel-heading">
            Wydarzenia na profilu
        </div>
        <div class="panel-body">
            
            <?php foreach($actions as $action): ?>
                <?php if($action->automat): ?>
                    <p class="text-center">{{ $action->content }}</p>
                <?php else: ?>
				<div class="col-sm-11">
					<ul class="nav navbar-nav navbar-right">
                        <p>{{ $action->created_at->format('Y-m-d H:i:s') }}</p>
					</ul>
				</div>
                    <div class="alert alert-info" role="alert">
                        <div class="row">
							<div class="col-sm-12">
									<p>{{ $action->content }}</p>
							</div>
                        </div>
                        <?php if($action->photos->count()): ?>
                            <div class="row">
                                <hr>
                                <?php foreach($action->photos as $photo): ?>
                                    <div class="col-xs-12 col-xs-12 ">
                                        <img src="{{ $photo->url }}" alt="{{ $photo->filename }}" class="img-responsive img-rounded">
                                    </div>
                                <?php endforeach; ?>
                            </div>
                        <?php endif; ?>
                    </div>
                <?php endif; ?>
            <?php endforeach; ?>
            <div class="text-center">
                <?= $actions->links() ?>
            </div>
        </div>
    </div>
</div>
@endsection
