@extends('layouts.dashboard')

@section('view')
    {{ csrf_field() }}
    <h2>Posiłki</h2>
    <div class="row">
        <div class="col-xs-12 text-right">
            <a href="{{ route('meals.create') }}" class="btn btn-primary">Dodaj</a>
        </div>
    </div>
    <table class="table table-sortable">
        <thead>
            <tr>
                <th></th>
                <th>kcal</th>
                <th></th>
            </tr>
        </thead>
        <tbody>
            <?php $last = ''; ?>
            <?php foreach($items as $item): ?>
                <?php $formatted =  $item->created_at->format('Y-m-d'); ?>
                <?php if($last !== $formatted): ?>
                    <?php $last = $formatted; ?>
                    <tr>
                        <td class="warning" colspan="3">
                            <strong>
                                {{ $formatted }}
                            </strong>
                        </td>
                    </tr>
                <?php endif; ?>
                <tr data-id="{{ $item->id }}">
                    <td>
                        {{ $item->created_at->format('H:i:s') }}
                    </td>
                    <td>{{ $item->kcal_text }}</td>
                    <td class="text-right">
                        <a href="#" class="btn btn-sm toggle-bottom-row">
                            <span class="glyphicon glyphicon-chevron-up glyphicon-chevron-down"></span>
                        </a>
                    </td>
                </tr>
                <tr class="hidden" data-id="{{ $item->id }}">
                    <td colspan="3">
                        <table class="table table-sortable">
                            <?php foreach($item->foods as $food): ?>
                                <tr class="info">
                                    <td>{{ $food->product->name }}</td>
                                    <td>{{ $food->kcal_text }}</td>
                                </tr>
                            <?php endforeach; ?>
                        </table>
                    </td>
                </tr>
            <?php endforeach; ?>
        </tbody>
    </table>

    


@endsection
