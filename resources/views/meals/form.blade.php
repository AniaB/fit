{{ csrf_field() }}

<div id="create-meal">
    <table class="hidden">
        <tr>
            <td data-product>
                <span></span>
                <input type="hidden" name="product_id[]" value="">
            </td>
            <td data-amount>
                <span></span>
                <input type="hidden" name="amount[]" value="">
            </td>
            <td>
                <button type="button" class="btn btn-danger btn-sm remove-row">
                    <span class="glyphicon glyphicon-trash"></span>
                </button>
            </td>
        </tr>
    </table>
    <table id="results" class="table table-striped">
        <thead>
            <tr>
                <td>Produkt</td>
                <td>Ilość (w gramach)</td>
                <td></td>
            </tr>
        </thead>
        <tbody></tbody>
    </table>
    
    <hr>
    
    <div class="row">
        <div class="col-sm-4 col-xs-12">
            @include('partials.forms.select', ['item' => @$item, 'label' => 'Nazwa', 'option' => 'productdata', 'source' => $_products])
        </div>
        <div class="col-sm-4 col-xs-12">
            @include('partials.forms.text', ['item' => @$item, 'label' => 'Ilość (g)', 'option' => 'amountdata'])
        </div>
        <div class="col-sm-4 col-xs-12">
            <div class="form-group">
                <label class="control-label">&nbsp;</label> 
                <div></div>
                @include('partials.forms.submit', ['text' => 'Dodaj'])
            </div>
        </div>
    </div>
</div>