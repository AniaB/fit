@extends('layouts.dashboard')

@section('view')
    <h2>Nowy posiłek</h2>
    <form action="{{ route('meals.store') }}" method="POST">
        @include('meals.form', ['item' => NULL])
        <div class="row">
            <div class="col-xs-12 text-right" >
                @include('partials.forms.submit', ['text' => 'Zapisz'])
            </div>
        </div>
        
    </form>
@endsection
