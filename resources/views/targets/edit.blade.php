@extends('layouts.dashboard')

@section('view')
    <h2>Zmień cel</h2>
    <form action="{{ route('targets.update', ['target' => $item]) }}" method="POST">
        @include('targets.form', ['item' => $item])
        {{ method_field('PATCH') }}
        <div class="row">
            <div class="col-xs-12 text-right" >
                @include('partials.forms.submit', ['text' => 'Zaktualizuj'])
            </div>
        </div>
        
    </form>
@endsection
