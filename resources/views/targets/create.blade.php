@extends('layouts.dashboard')

@section('view')
    <h2>Nowy cel</h2>
    <form action="{{ route('targets.store') }}" method="POST">
        @include('targets.form', ['item' => NULL])
        <div class="row">
            <div class="col-xs-12 text-right" >
                @include('partials.forms.submit', ['text' => 'Zapisz'])
            </div>
        </div>
        
    </form>
@endsection
