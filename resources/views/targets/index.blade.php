@extends('layouts.dashboard')

@section('view')
    {{ csrf_field() }}
    <h2>Cele</h2>
    <div class="row">
        <div class="col-xs-12 text-right">
            <a href="{{ route('targets.create') }}" class="btn btn-primary">Dodaj</a>
        </div>
    </div>
    <table class="table table-sortable">
        <thead>
            <tr>
                <th></th>  <th></th>
                <th>Nazwa</th>
                <th></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach($items as $item): ?>
                <tr data-id="{{ $item->id }}">
                    <td>
                        <div class="btn btn-xs">
                            <span class="glyphicon glyphicon-menu-hamburger"></span>
                        </div>
                    </td>
                    <td>{{ $item->deadline->format('Y-m-d') }}</td>
                    <td>{{ $item->name }}</td>
                    <td class="text-right">
                        <form action="{{ route('targets.destroy', ['target' => $item]) }}" method="POST">
                            {{ method_field('DELETE') }}
                            {{ csrf_field() }}
                            <a href="{{ route('targets.toggle', ['target' => $item]) }}" class="btn btn-success {{ $item->active ? '' : 'btn-outline' }} btn-sm">
                                <span class="glyphicon glyphicon-ok"></span>
                            </a>
                            <?php if(!$item->active): ?>
                                <a href="{{ route('targets.edit', ['target' => $item]) }}" class="btn btn-primary btn-sm">edytuj</a>
                                @include('partials.forms.submit', ['text' => 'usuń', 'class' => 'btn-sm btn-danger'])
                            <?php endif; ?>
                        </form>
                    </td>
                </tr>
            <?php endforeach; ?>
        </tbody>
    </table>

@endsection
