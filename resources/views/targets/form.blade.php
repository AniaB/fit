{{ csrf_field() }}
<div class="row">
    <div class="col-sm-6 col-xs-12">
        @include('partials.forms.text', ['item' => @$item, 'label' => 'Nazwa', 'option' => 'name'])
    </div>
    <div class="col-sm-6 col-xs-12">
        @include('partials.forms.text', ['type' => 'number', 'item' => @$item, 'label' => 'Docelowa waga', 'option' => 'weight'])
    </div>
    <div class="col-sm-6 col-xs-12">
        @include('partials.forms.datepicker', ['type' => 'text', 'item' => @$item, 'label' => 'Do kiedy', 'option' => 'deadline'])
    </div>
    <div class="col-sm-6 col-xs-12">
        @include('partials.forms.textarea', ['item' => @$item, 'label' => 'Opis', 'option' => 'description'])
    </div>
</div>
