@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="row">
            <div class="col-md-3 col-sm-4 col-xs-12">
                <div id="dashboard-menu">
                    <div class="panel panel-default panel-user">
                        <div class="panel-heading">{{ $user->login }}</div>
                        <div id="avatar" style="background-image:url('{{ $user->avatar }}')"></div>
                        <div class="panel-body">
                            <p>waga: <strong>{{ $user->weight }}</strong> {{ $user->wunit }}</p>
                            <p>wzrost: <strong>{{ $user->height }}</strong> {{ $user->hunit }}</p>
                            <p>BMI: <strong>{{ $user->bmi }}</strong>
                                <?php if($user->bmi_state === -1): ?>
									<font color="yellow">(niedowaga)</font>
                                <?php elseif($user->bmi_state === 0): ?>
                                    <font color="green">(waga prawidłowa)</font>
                                <?php elseif($user->bmi_state === 1): ?>
                                    <font color="red">(nadwaga)</font>
                                <?php endif; ?>
                            </p>
                            <p>wiek: <strong>{{ $user->age }}</strong></p>
                        </div>
                    </div>
                    <ul class="nav nav-pills nav-stacked">
                        <li role="presentation" class="{{ request()->is('profile') ? 'active' : '' }}">
                            <a href="{{ route('profile') }}">Strona główna panelu</a>
                        </li>
                        <li role="presentation" class="{{ request()->is('profile/targets*') ? 'active' : '' }}">
                            <a href="{{ route('targets.index') }}">Cele</a>
                        </li>
                        <li role="presentation" class="{{ request()->is('profile/trainings*') ? 'active' : '' }}">
                            <a href="{{ route('trainings.index') }}">Treningi</a>
                        </li>
                        <li role="presentation" class="{{ request()->is('profile/meals*') ? 'active' : '' }}">
                            <a href="{{ route('meals.index') }}">Dieta</a>
                        </li>
                        <li role="presentation" class="{{ request()->is('profile/messages*') ? 'active' : '' }}">
						
							<a href="{{ route('messages.index') }}">Wiadomości <!--<span class="badge">{{ auth()->user()->messagesOf($user)->count() }}</span> -->
							</a>
							
						</li>
                        <li role="presentation" class="{{ request()->is('posts*') ? 'active' : '' }}">
                            <a href="{{ route('posts.index') }}">Posty</a>
                        </li>
                        <li role="presentation" class="{{ request()->is('profile/settings*') ? 'active' : '' }}">
                            <a href="{{ route('profile.settings') }}">Ustawienia</a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="col-md-9 col-sm-8 col-xs-12">
                @include('partials.errors')
                @yield('view')
            </div>
        </div>
    </div>
@endsection
