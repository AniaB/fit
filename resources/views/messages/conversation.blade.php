@extends('layouts.dashboard')

@section('view')

    <h2>Korespondencja z {{ $recipient->full_name }}</h2>

    <?php foreach($messages as $message): ?>
        <div class="row">
            <div class="col-xs-8 {{ $message->author->id == auth()->user()->id ? 'pull-right' : '' }}">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <span class="glyphicon glyphicon-chevron-{{ $message->author->id == auth()->user()->id ? 'up' : 'down' }}"></span>
                        {{ $message->created_at->format('Y-m-d H:i:s') }}
                    </div>

                    <div class="panel-body">
                        {{ $message->content }}
                    </div>
                </div>
            </div>
        </div>
    <?php endforeach; ?>

    <form action="{{ route('messages.send', ['user' => $recipient]) }}" method="POST">
        <hr>
        {{ csrf_field() }}

        <div class="row">
            <div class="col-xs-12 new-message">
                @include('partials.forms.textarea', ['item' => NULL, 'label' => count($messages) ? 'Odpowiedź' : 'Rozpocznij konwersację', 'option' => 'content'])
            </div>
            <div class="col-xs-12 text-right">
                @include('partials.forms.submit', ['text' => 'Wyślij'])
            </div>
        </div>
    </form>

@endsection
