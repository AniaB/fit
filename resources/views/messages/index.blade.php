@extends('layouts.dashboard')

@section('view')
    <h2>Wiadomości</h2>
    <table class="table table-sortable">
        <tbody>
            <?php foreach($items as $item): ?>
                <tr data-id="{{ $item->id }}">
                    <td>
                        <div class="row">
                            <div class="col-sm-10 col-xs-12">
                                <a href="{{ route('messages.conversation', ['user' => $item->recipient]) }}">
                                    {{ $item->recipient->full_name }}
                                </a>
                            </div>
                            <div class="col-sm-2 hidden-xs text-right">
                                ({{ auth()->user()->messagesOf($item->recipient)->count() }})
                            </div>
                        </div>
                    </td>
                </tr>
            <?php endforeach; ?> 
        </tbody>
    </table>
@endsection
