@extends('layouts.dashboard')

@section('view')
    <h2>Nowy trening</h2>
    <form action="{{ route('trainings.store') }}" method="POST">
        @include('trainings.form', ['item' => NULL])
        <div class="row">
            <div class="col-xs-12 text-right" >
                @include('partials.forms.submit', ['text' => 'Zapisz'])
            </div>
        </div>
        
    </form>
@endsection
