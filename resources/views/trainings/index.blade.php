@extends('layouts.dashboard')

@section('view')
    {{ csrf_field() }}
    <h2>Twoje treningi</h2>
    <div class="row">
        <div class="col-xs-12 text-right">
            <a href="{{ route('trainings.create') }}" class="btn btn-primary">Dodaj</a>
        </div>
    </div>
    <table class="table table-sortable">
        <tbody>
            <?php $last = ''; ?>
            <?php foreach($items as $item): ?>
                <?php $formatted =  $item->created_at->format('Y-m-d'); ?>
                <?php if($last !== $formatted): ?>
                <?php $last = $formatted; ?>
                    <tr>
                        <td class="warning" colspan="5">
                            <strong>
                                {{ $formatted }}
                            </strong>
                        </td>
                    </tr>
                <?php endif; ?>
                <tr>
                    <td>
                        {{ $item->created_at->format('H:i') }}
                    </td>
                    <td>{{ $item->sport->name }}</td>
                    <td><i class="glyphicon glyphicon-time"></i> {{ $item->duration_text }}</td>
                    <td>{{ $item->kcal_text }}</td>
                    <td class="text-right">
                        <form action="{{ route('trainings.destroy', ['training' => $item]) }}" method="POST">
                            {{ method_field('DELETE') }}
                            {{ csrf_field() }}
                            <?php if($item->editable): ?>
                                <a href="{{ route('trainings.edit', ['target' => $item]) }}" class="btn btn-primary btn-sm">edytuj</a>
                                @include('partials.forms.submit', ['text' => 'usuń', 'class' => 'btn-sm btn-danger'])
                            <?php endif; ?>
                        </form>
                    </td>
                </tr>
            <?php endforeach; ?>
        </tbody>
    </table>

    


@endsection
