{{ csrf_field() }}
<div class="row">
    <div class="col-xs-6">
        @include('partials.forms.select', ['item' => @$item, 'label' => 'Sport', 'option' => 'sport_id', 'source' => $sports])
    </div>
    <div class="col-xs-6">
        @include('partials.forms.text', ['item' => @$item, 'label' => 'Czas trwania (minuty)', 'option' => 'duration'])
    </div>
    <div class="col-xs-12">
        @include('partials.forms.textarea', ['item' => @$item, 'label' => 'Opis', 'option' => 'description'])
    </div>
</div>