<?php

namespace App;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

use Carbon\Carbon;

class Meal extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'description'
    ];
    
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
    ];
    
    /**
     * Fieldsto be casted for Carbon dates
     *
     * @var array
     */
    protected $dates = [
        'created_at', 'updated_at',
    ];
    
    public function user() {
        return $this->belongsTo('\App\User');
    }
    
    public function foods() {
        return $this->hasMany('\App\Food');
    }
    
    public function getKcalAttribute() {
        $sum = 0;
        foreach($this->foods as $food)
        {
            $sum += $food->kcal;
        }

        return $sum;
    }
    
    public function getKcalTextAttribute() {
        return $this->kcal . ' kcal';
    }
    
}
