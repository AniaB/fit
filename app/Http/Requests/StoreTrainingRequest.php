<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

use Auth;

use \App\Training;

class StoreTrainingRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'sport_id'  => 'required|exists:sports,id',
            'duration'  => 'required|numeric|min:10',
        ];
        
        if($this->method() == 'PATCH')
        {
        }
        
        return $rules;
    }
    
    public function messages()
    {
        return [
            'sport_id.*'            => 'Popraw pole Sport',
            'duration.*'            => 'Czas trwania powinien być dłuższy niż 10 minut',
        ];
        
    }
}
