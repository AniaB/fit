<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

use Auth;

use \App\User;

class StoreUserRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'name'              => 'required',
//            'lastname'          => 'required',
        ];
        
        if($this->input('password'))
        {
            $rules['password']  = 'min:6';
            $rules['password2'] = 'required|same:password';
        }
        
        if($this->file('avatar'))
        {
            $rules['avatar'] = 'required|image|mimes:jpeg,png';
        }
        
        if($this->method() == 'PATCH')
        {
//            $rules['login']     = 'required|alpha_num|unique:users,login,'.(\Route::current()->getParameter('users')->id);
//            $rules['email']     = 'required|email|unique:users,email,'.(\Route::current()->getParameter('users')->id);
        }
        
        return $rules;
    }
    
    public function messages()
    {
        return [
            'name.*'            => 'Podaj imię',
            'lastname.*'        => 'Podaj nazwisko',
            
            'avatar.*'          => 'Awatar powinien być plikiem graficznym (JPG lub PNG)',
            
            'password.min'      => 'Hasło powinno mieć przynajmiej 6 znaków',
            'password.*'        => 'Podaj hasło',
            'password2.same'    => 'Hasła się nie zgadzają',
            'password2.*'       => 'Powtórz hasło',
        ];
        
    }
}
