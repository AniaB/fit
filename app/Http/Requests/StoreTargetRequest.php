<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

use Auth;

use \App\Target;

class StoreTargetRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'name'      => 'required',
            'deadline'  => 'required|date|after:today',
            'weight'    => 'required|numeric|min:1',
        ];
        
        if($this->method() == 'PATCH')
        {
        }
        
        return $rules;
    }
    
    public function messages()
    {
        return [
            'name.*'        => 'Podaj nazwę',
            'deadline.*'    => 'Podaj poprawną datę (późniejszą niż dzisiejsza)',
            'weight.*'      => 'Podaj poprawną wagę (większą niż 0)',
        ];
        
    }
}
