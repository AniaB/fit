<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

use Auth;

use \App\Message;

class StoreMessageRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'content'   => 'required',
        ];
        
        if($this->method() == 'PATCH')
        {
        }
        
        return $rules;
    }
    
    public function messages()
    {
        return [
            'content.*'     => 'Podaj treść wiadomości',
        ];
        
    }
}
