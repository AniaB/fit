<?php

namespace App\Http\Controllers;

use \Storage;
use App\Message;
use Illuminate\Http\Request;
use \App\Http\Requests\StoreMessageRequest;

class MessagesController extends Controller
{
    /**
     * Display main view.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('messages.index')->with([
            'items' => auth()->user()->conversations
        ]);
    }
    
    public function conversation(Request $request, $user)
    {
        if($user->id === auth()->user()->id)
            return back()->with('error', 'Nie możesz wysłać wiadomości sam do siebie');
        
        return view('messages.conversation')->with([
            'messages' => auth()->user()->messagesOf($user)->get(),
            'recipient' => $user
        ]);
    }
    
    public function send (StoreMessageRequest $request, $user)
    {
        if($user->id === auth()->user()->id)
            return back()->with('error', 'Nie możesz wysłać wiadomości sam do siebie');
        
        $input = $request->only(['content']);
        $input['recipient_id'] = $user->id;
        auth()->user()->messages()->create($input);
        return back();
    }
	
	
}
