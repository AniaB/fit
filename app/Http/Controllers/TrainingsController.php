<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use \App\Training;

use \Auth;

use \App\Http\Requests\StoreTrainingRequest;

class TrainingsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('trainings.index')->with([
            'items' => auth()->user()->trainings
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('trainings.create')->with([
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreTrainingRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreTrainingRequest $request)
    {
        $input = $request->only(['sport_id', 'duration', 'description']);
        auth()->user()->trainings()->create($input);
        return redirect()->route('trainings.index')->with(['success' => 'Trening zapisano. Możesz go edytować póki nie zmienisz parametrów ciała']);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Target $item
     * @return \Illuminate\Http\Response
     */
    public function show($item)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Target $item
     * @return \Illuminate\Http\Response
     */
    public function edit($item)
    {
        if($item->user_id !== auth()->user()->id)
            return back();
        
        return view('trainings.edit')->with([
            'item' => $item,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\StoreTrainingRequest  $request
     * @param  \App\Training $item
     * @return \Illuminate\Http\Response
     */
    public function update(StoreTrainingRequest $request, $item)
    {
        $input = $request->only(['sport_id', 'description', 'duration']);
        $item->update($input);
        return redirect()->route('trainings.index')->with(['success' => 'Trening został zaktualiwany']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Training $item
     * @return \Illuminate\Http\Response
     */
    public function destroy($item)
    {
        $item->delete();
        return redirect()->route('trainings.index')->with(['success' => 'Trening został wycofany']);
    }
    
    public function reorder(Request $request)
    {
        foreach($request->input('order') as $order => $id)
            Target::where('id', $id)->update(['position' => $order]);
        die;
    }
    
    public function toggle(Request $request, $item)
    {
        $item->active = (int)!$item->active;
        $item->save();
        return back();
    }
}
