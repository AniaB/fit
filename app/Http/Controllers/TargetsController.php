<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use \App\Target;

use \Auth;

use \App\Http\Requests\StoreTargetRequest;

class TargetsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('targets.index')->with([
            'items' => auth()->user()->targets
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('targets.create')->with([
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreTargetRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreTargetRequest $request)
    {
        $input = $request->only(['name', 'description', 'weight', 'deadline']);
        auth()->user()->targets()->create($input);
        return redirect()->route('targets.index')->with(['success' => 'Cel został dodany']);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Target $item
     * @return \Illuminate\Http\Response
     */
    public function show($item)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Target $item
     * @return \Illuminate\Http\Response
     */
    public function edit($item)
    {
        if($item->user_id !== auth()->user()->id)
            return back();
        
        return view('targets.edit')->with([
            'item' => $item,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\StoreTargetRequest  $request
     * @param  \App\Target $item
     * @return \Illuminate\Http\Response
     */
    public function update(StoreTargetRequest $request, $item)
    {
        $input = $request->only(['name', 'description', 'weight', 'deadline']);
        
        $item->update($input);
        return redirect()->route('targets.index')->with(['success' => 'Cel został zaktualiwany']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Target $items
     * @return \Illuminate\Http\Response
     */
    public function destroy($item)
    {
        $item->delete();
        return redirect()->route('targets.index')->with(['success' => 'Cel został usunięty']);
    }
    
    public function reorder(Request $request)
    {
        foreach($request->input('order') as $order => $id)
            Target::where('id', $id)->update(['position' => $order]);
        die;
    }
    
    public function toggle(Request $request, $item)
    {
        foreach(auth()->user()->targets as $target)
            $target->deactivate(FALSE);
        
        $item->active ? $item->deactivate() : $item->activate();
        
        return back();
    }
}
