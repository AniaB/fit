<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \App\User;

class IndexController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('index.index')->with([
            'users' => User::all()
        ]);
    }
    
    public function user(Request $request, $user)
    {
        return view('index.user')->with([
            'user' => $user,
            'actions' => $user->actions()->paginate(25)
        ]);
    }
}
