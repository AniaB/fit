<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use \App\Action;

use \Auth;

use \App\Http\Requests\StoreActionRequest;

class PostsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('posts.index')->with([
            'items' => auth()->user()->actions()->where('automat', 0)->get()
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('posts.create')->with([
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreActionRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreActionRequest $request)
    {
        $input = $request->only(['content']);
        $input['automat'] = 0;
        $item = auth()->user()->actions()->create($input);


        foreach($request->file(['files']) as $upload)
        {
            $file = \Storage::putFile('public/photos', $upload);
            $item->photos()->create([
                'filename' => basename($file)
            ]);
        }
        return redirect()->route('posts.index')->with(['success' => 'Post został zapisany']);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Action $item
     * @return \Illuminate\Http\Response
     */
    public function show($item)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Action $item
     * @return \Illuminate\Http\Response
     */
//    public function edit($item)
//    {
//        if($item->user_id !== auth()->user()->id)
//            return back();
//        
//        return view('posts.edit')->with([
//            'item' => $item,
//        ]);
//    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\StoreActionRequest  $request
     * @param  \App\Action $item
     * @return \Illuminate\Http\Response
     */
//    public function update(StoreActionRequest $request, $item)
//    {
//        $input = $request->only(['content']);
//        
//        $item->update($input);
//        return redirect()->route('posts.index')->with(['success' => 'Post został zaktualizowany']);
//    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Action $items
     * @return \Illuminate\Http\Response
     */
    public function destroy($item)
    {
        $item->delete();
        return redirect()->route('posts.index')->with(['success' => 'Post został usunięty']);
    }
}
