<?php

namespace App\Http\Controllers;

use \Storage;
use App\User;
use \App\Http\Requests\StoreUserRequest;

class ProfileController extends Controller
{
    /**
     * Display main view.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {        
        if(auth()->user()->updated_at->diff(new \Carbon\Carbon)->days > config('app.update_every'))
            return redirect()->route('profile.settings')->with([
                'warning' => 'Od ponad '.config('app.update_every').' dni nie aktualizowałeś swoich danych. Zaktualizuj je teraz'
            ]);
        
        return view('profile.index')->with([
            'user' => auth()->user()
        ]);
    }
    
    public function settings()
    {
        return view('profile.settings')->with([
            'user' => auth()->user()
        ]);
    }
    
    public function update(StoreUserRequest $request)
    {
        $fields = ['name', 'lastname', 'login', 'gender', 'year', 'weight', 'height', 'avatar','wallpaper'];
        
        if($request->has('password'))
        {
            $fields[] = 'password';
            $request->merge(['password' => bcrypt($request->input('password'))]);
        }
        
        if($request->file('avatar'))
        {
            Storage::disk('public')->delete([
                'avatars/'.auth()->user()->id.'.jpg', 
                'avatars/'.auth()->user()->id.'.jpeg', 
                'avatars/'.auth()->user()->id.'.png', 
            ]);
            Storage::disk('public')->putFileAs('avatars', $request->file('avatar'), auth()->user()->id.'.'.$request->file('avatar')->getClientOriginalExtension());
        }
        
        if($request->file('wallpaper'))
        {
            Storage::disk('public')->delete([
                'wallpaper/'.auth()->user()->id.'.jpg',
                'wallpaper/'.auth()->user()->id.'.jpeg', 
                'wallpaper/'.auth()->user()->id.'.png', 
            ]);
            Storage::disk('public')->putFileAs('wallpaper', $request->file('wallpaper'), auth()->user()->id.'.'.$request->file('wallpaper')->getClientOriginalExtension());
        }
        
        $input = $request->only($fields);
        auth()->user()->touch();
        auth()->user()->update($input);
        auth()->user()->trainings()->update([
            'editable' => 0
        ]);
        
        return back()->with([
            'success' => 'Dane zostały zaktualizowane'
        ]);
    }
    
}
