<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use \App\Meal;

use \Auth;

use \App\Http\Requests\StoreMealRequest;

class MealsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('meals.index')->with([
            'items' => auth()->user()->meals
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('meals.create')->with([
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreMealRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreMealRequest $request)
    {
        $input = $request->only(['product_id', 'amount']);
        $meal = auth()->user()->meals()->create([]);
        foreach($input['product_id'] as $key => $value)
        {
            if($value)
            {
                $meal->foods()->create([
                   'product_id' => $value,
                   'amount' => $input['amount'][$key]
                ]);
            }
        }
        return redirect()->route('meals.index')->with(['success' => 'Posiłek został zapisany']);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Meal $item
     * @return \Illuminate\Http\Response
     */
    public function show($item)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Meal $item
     * @return \Illuminate\Http\Response
     */
    public function edit($item)
    {
        if($item->user_id !== auth()->user()->id)
            return back();
        
        return view('meals.edit')->with([
            'item' => $item,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\StoreMealRequest  $request
     * @param  \App\Meal $item
     * @return \Illuminate\Http\Response
     */
    public function update(StoreMealRequest $request, $item)
    {
        $input = $request->only(['name']);
        
        $item->update($input);
        return redirect()->route('meals.index')->with(['success' => 'Posiłek został zaktualiwany']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Meal $items
     * @return \Illuminate\Http\Response
     */
    public function destroy($item)
    {
        $item->delete();
        return redirect()->route('meals.index')->with(['success' => 'Posiłek został usunięty']);
    }
    
    public function reorder(Request $request)
    {
        foreach($request->input('order') as $order => $id)
            Meal::where('id', $id)->update(['position' => $order]);
        die;
    }
    
    public function toggle(Request $request, $item)
    {
        $item->active = (int)!$item->active;
        $item->save();
        return back();
    }
}
