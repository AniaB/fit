<?php

namespace App\Http\Middleware;

use Closure;

class Permissions
{

    public function handle($request, Closure $next)
    {
//        dd('permission checking!');
//        dd($request->route()->getName());
        if (!$request->user()->hasPermission($request->route()->getName())) {
            return redirect()->back()->withErrors('Nie posiadasz uprawnień do żądanej części systemu');
        }

        return $next($request);
    }
}