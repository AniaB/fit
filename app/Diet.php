<?php

namespace App;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

use Carbon\Carbon;

class Diet extends Model
{
    
    /**
     * The "booting" method of the model.
     *
     * @return void
     */
    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope('order', function (Builder $builder) {
            $builder->orderBy('created_at', 'DESC');
        });
    }
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'description', 'meat', 'kcal'
    ];
    
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
    ];
    
    /**
     * Fieldsto be casted for Carbon dates
     *
     * @var array
     */
    protected $dates = [
        'created_at', 'updated_at',
    ];
    
    public function user() {
        return $this->belongsTo('\App\User');
    }
    
    public function sport() {
        return $this->belongsTo('\App\Sport');
    }
    
    public function getDurationTextAttribute(){
        $carbon = Carbon::createFromTimestamp($this->duration*60);        
        return $carbon->format("H:i \h");
    }
    
    public function getKcalTextAttribute(){
        return $this->kcal . ' kcal';
    }
    
}
