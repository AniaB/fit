<?php

namespace App\Providers;

use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;

class ComposerServiceProvider extends ServiceProvider
{

    /**
     * Register bindings in the container.
     *
     * @return void
     */
    public function boot()
    {
        view()->composer('layouts.dashboard', function ($view) {
            $view->with('user', auth()->user());
        });
        
        view()->composer('trainings.form', function ($view) {
            $view->with('sports', \App\Sport::pluck('name', 'id'));
        });
        
        view()->composer('meals.form', function ($view) {
            $view->with('_products', \App\Product::pluck('name', 'id'));
        });
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        //
    }

}