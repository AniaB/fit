<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Http\Request;
use App\User;
use App\Target;
use App\Training;
use App\Photo;
use App\Action;
use App\Observers\UserObserver;
use App\Observers\TargetObserver;
use App\Observers\TrainingObserver;
use App\Observers\PhotoObserver;
use App\Observers\ActionObserver;
use \Auth;
use \Illuminate\Support\Facades\Route;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot(Request $request)
    {
        User::observe(UserObserver::class);
        Target::observe(TargetObserver::class);
        Training::observe(TrainingObserver::class);
        Photo::observe(PhotoObserver::class);
        Action::observe(ActionObserver::class);
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
