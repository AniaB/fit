<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

function percentage($value, $of, $decimals = 2)
{
    return number_format($value * 100 / $of, $decimals);
}

function gender_text($man, $woman, $user = NULL)
{
    $user = $user ?: auth()->user();
    
    return $user->gender ? $woman : $man;
}

