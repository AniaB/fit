<?php

namespace App;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use \Carbon\Carbon;

class Target extends Model
{
    
    /**
     * The "booting" method of the model.
     *
     * @return void
     */
    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope('position', function (Builder $builder) {
            $builder->orderBy('position', 'ASC');
        });
    }
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'description', 'deadline', 'weight', 'difference', 'daily_info', 'daily_burnout', 'activated_at', 'start_weight'
    ];
    
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
    ];
    
    /**
     * Fieldsto be casted for Carbon dates
     *
     * @var array
     */
    protected $dates = [
        'created_at', 'updated_at', 'deadline'
    ];
    
    public function user() 
    {
        return $this->belongsTo('\App\User');
    }
    
    public function getBurnoutAttribute() 
    {
        return $this->difference * \App\Algorithm::KCAL_PER_KG;
    }
    
    public function getDurationAttribute() 
    {
        return $this->created_at->diffInDays($this->deadline);
    }
//    
//    public function getDailyBurnoutAttribute() 
//    {
//        return round($this->burnout/$this->duration);
//    }
    
    public function getIsPastAttribute() 
    {
        return Carbon::now() >= $this->deadlione;
    }
    
    public function activate($message = TRUE) 
    {
        
        $this->active = 1;
        $this->finished = 0;
        $this->activated_at = Carbon::now();
        $this->start_weight = auth()->user()->weight;
        $this->difference = $this->user->weight - $this->weight;
        $this->daily_burnout = round($this->burnout / $this->duration);
        $this->save();
        
        if($message)
        {
            $this->user->actions()->create([
                'content' => 'Nowy cel: '.$this->name
            ]);
        }
        
    }
    
    public function deactivate($message = TRUE) 
    {
        $this->active = 0;
        $this->finished = 0;
        $this->activated_at = NULL;
        $this->start_weight = 0;
        $this->daily_burnout = 0; //kcal
        $this->difference = 0;
        $this->save();
        
        if($message)
        {
            $this->user->actions()->create([
                'content' => 'Usunięcie celu'
            ]);
        }
    }
    
}
