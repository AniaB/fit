<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use \App\User;
use \App\Algorithm;
use \Carbon\Carbon;

class Diet extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'diets';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Recalculate diets for every present user';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $users = User::all();
        
        foreach($users as $user)
        {
            if($user->current_target !== NULL)
            {
                
                $algorithm = new Algorithm($user, (new Carbon)->subDay());
                $algorithm->calculate();
                $this->comment('user '.($user->id).' parsed');
            }
        }
        
//        $this->comment();
    }
}
