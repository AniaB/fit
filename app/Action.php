<?php

namespace App;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

use Carbon\Carbon;

class Action extends Model
{
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id', 'content', 'automat'
    ];
    
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
    ];
    
    public function user() {
        return $this->belongsTo('\App\User');
    }
    
    public function photos() {
        return $this->hasMany('\App\Photo');
    }
    
    public function getLeadAttribute() 
    {
        return str_limit($this->content, 100);
    }
    
}
