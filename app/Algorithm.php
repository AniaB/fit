<?php

namespace App;
use Carbon\Carbon;

class Algorithm 
{
    protected $_user;
    protected $_day;
    
    const KCAL_PER_KG = 7000;
    
    public function __construct(User $user, Carbon $day)
    {
        $this->_user = $user;
        $this->_day = $day;
    }
    
    public function calculate()
    {
        $meals = $this->_user->meals()->whereDate('created_at', '=', $this->_day->toDateString())->get();
        $kcal_eaten = $meals->sum('kcal');
        
        $trainings = $this->_user->trainings()->whereDate('created_at', '=', $this->_day->toDateString())->get();
        $kcal_burned = $trainings->sum('kcal');
        
        $should_daily_kcal = $this->_user->daily_kcal;
        $should_daily_burn = $this->_user->current_target->daily_burnout;
        
        $diff = $kcal_eaten - $kcal_burned;
        
        if(($should_daily_kcal - $diff) >= $should_daily_burn)
        {
            $this->_user->current_target->daily_info = $this->composeMessage(1);
            $this->_user->current_target->save();
        }
        else
        {
            $this->_user->current_target->daily_info = $this->composeMessage(0);
            
            $this->_user->current_target->save();
        }
        $this->_user->actions()->create([
            'content' => $this->_user->current_target->daily_info
        ]);
        
        if($this->_day->format('Y-m-d') === $this->_user->current_target->deadline->format('Y-m-d'))
        {
            $info = 'Ukończony cel '.$this->_user->current_target->name.'!';
            $this->_user->current_target->deactivate(FALSE);
            $this->_user->current_target->finished = 1;
            $this->_user->current_target->daily_info = $info;
            $this->_user->current_target->save();
            $this->_user->actions()->create([
                'content' => $info
            ]);
        }
    }
    
    protected function composeMessage($success, $amount = 0)
    {
        if($success)
            return 'Świetnie! W dniu '. $this->_day->format('Y-m-d') .' udało Ci się spalić więcej kalorii niż należało! Tak trzymać!';
        else
            return 'Oj popraw się! W dniu '. $this->_day->format('Y-m-d') .' nie udało Ci się spalić odpowiedniej ilości kalorii';
    }
    
    public static function trainingKcal(\App\Sport $sport, int $minutes)
    {
        $division = $minutes / 60;
        return round($sport->burns * $division);
    }
    
}
