<?php

namespace App;
use Illuminate\Database\Eloquent\Model;
use \Carbon\Carbon;

class Message extends Model
{
	/**
     * The attributes that are mass assignable.
     * @var array
     */
    protected $fillable = [
        'user_id', 'recipient_id', 'content'
    ];
    
    /**
     * The attributes that should be hidden for arrays.
     * @var array
     */
    protected $hidden = [
    ];
    
    /**
     * Fieldsto be casted for Carbon dates
     * @var array
     */
    protected $dates = [
        'created_at', 'updated_at', 'read_at',
    ];
    
    /**
     * Author
     * @return App\User
     */
    public function author() 
    { 
        return $this->belongsTo('App\User', 'user_id');
    }
    
    /**
     * Author
     * @return App\User
     */
    public function recipient() 
    { 
        return $this->belongsTo('App\User');
    }
    
}
