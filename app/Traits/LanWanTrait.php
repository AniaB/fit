<?php

namespace App\Traits;


trait LanWanTrait
{

    public function getAddress()
    {
        return \IP::create($this->address);
    }

    public function getMask()
    {
        $ip = \IPBlock::create($this->address, $this->mask);
        return $ip->getMask();
    }
    
    public function getFullMask()
    {
        return $this->getMask() . '/' . $this->mask;
    }
    
    public function getFullMaskInfo()
    {
        $ip = \IPBlock::create($this->address, $this->mask);
        return '<strong>' . $ip->getMask() . ' / ' . $this->mask . '</strong> ('.number_format($ip->getNbAddresses(), 0, ',', ' ').' adresów)';
    }
    
    /**
     * Interface type this LAN is attached to
     * 
     * @return string Type of interface that is sttached
     */
    public function interfaceType()
    {
        if ($this->physicalinterface)
            return 'physical';

        if ($this->virtualinterface)
            return 'virtual';

        return 'undefined';
    }
    
    /**
     * Interface this LAN is attached to
     * 
     * @return mixed
     */
    public function getInterface()
    {
        if ($this->physicalinterface)
            return $this->physicalinterface;

        if ($this->physicalinterface)
            return $this->physicalinterface;

        return NULL;
    }
    
    public static function availableMasks()
    {
        return [
            '16' => '255.255.0.0 (16)',
            '17' => '255.255.128.0 (17)',
            '18' => '255.255.192.0 (18)',
            '19' => '255.255.224.0 (19)',
            '20' => '255.255.240.0 (20)',
            '21' => '255.255.248.0 (21)',
            '22' => '255.255.252.0 (22)',
            '24' => '255.255.254.0 (24)',
            '25' => '255.255.255.128 (25)',
            '26' => '255.255.255.192 (26)',
            '27' => '255.255.255.224 (27)',
            '28' => '255.255.255.240 (28)',
            '29' => '255.255.255.248 (29)',
            '30' => '255.255.255.252 (30)',
            '31' => '255.255.255.254 (31)'
        ];
    }

}
