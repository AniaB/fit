<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;

use \Carbon\Carbon;

class User extends Authenticatable
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'login', 'email', 'password', 'name', 'lastname', 'last_logged', 'active', 'height','weight','year','gender'
    ];
    
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
    
    /**
     * Fieldsto be casted for Carbon dates
     *
     * @var array
     */
    protected $dates = [
        'created_at', 'updated_at', 'last_logged',
    ];
    
    public function getRouteKeyName()
    {
        return 'id';
    }
    
    /**
     * Messages of this user
     * 
     * @return App\Message
     */
    public function messages() 
    { 
        return $this->hasMany('App\Message');
    }
    
    /**
     * Correspondents
     * 
     * @return App\Message
     */
    public function conversations() 
    {
        return $this->hasMany('App\Message')->distinct()->orderBy('created_at', 'DESC')->groupBy('recipient_id');
    }
    
    /**
     * 
     * 
     * @return App\Message
     */
    public function messagesOf($correspondent) 
    {
        return $this->hasMany('App\Message')->where('recipient_id', $correspondent->id)->orWhere(function($query) use($correspondent) {
            $query->where('user_id', $correspondent->id)->where('recipient_id', auth()->user()->id);
        })->orderBy('created_at', 'ASC');
    }
    
    /**
     * Trainings of this user
     * 
     * @return App\Training
     */
    public function trainings() 
    { 
        return $this->hasMany('App\Training')->orderBy('created_at', 'DESC');
    }
    
    /**
     * Targets of this user
     * 
     * @return App\Target
     */
    public function targets() 
    { 
        return $this->hasMany('App\Target');
    }
    
    public function current_target() 
    { 
        return $this->hasOne('App\Target')->where('active', 1);
    }
    
    /**
     * Meals of this user
     * 
     * @return App\Meal
     */
    public function meals() 
    { 
        return $this->hasMany('App\Meal')->orderBy('created_at', 'DESC');
    }
    
    /**
     * Actions messages of this user
     * 
     * @return App\Action
     */
    public function actions() 
    { 
        return $this->hasMany('App\Action')->orderBy('created_at', 'DESC');
    }
    
    public function getAvatarAttribute()
    {
        $files = [
                'avatars/'.$this->id.'.jpg', 
                'avatars/'.$this->id.'.jpeg', 
                'avatars/'.$this->id.'.png', 
            ];
        
        foreach($files as $file)
            if(\Storage::disk('public')->exists($file))
                return route('avatar', ['avatar' => basename($file)]);
        
        return '/assets/images/user_'.($this->gender ? '' : 'fe').'male.png';
    }
    
    public function getWallpaperAttribute()
    {
        $files = [
                'wallpaper/'.$this->id.'.jpg', 
                'wallpaper/'.$this->id.'.jpeg', 
                'wallpaper/'.$this->id.'.png', 
            ];
        
        foreach($files as $file)
            if(\Storage::disk('public')->exists($file))
                return route('wallpaper', ['wallpaper' => basename($file)]);
        
        return '';
    }
    
    public function getFullNameAttribute()
    {
        return implode(' ', [$this->name, $this->lastname]);
    }
    
    public function getAnonymusNameAttribute()
    {
        return implode(' ', [$this->name, substr($this->lastname, 0, 1)]);
    }
    
    public function getAgeAttribute()
    {
        return (new \Carbon\Carbon)->format('Y') - $this->year;
    }
    
    public function getBmiAttribute()
    {
        return number_format($this->weight / pow($this->height / 100, 2), 2);
    }
    
    public function getBmiStateAttribute()
    {
        if($this->bmi < 18.5)
            return -1;
        elseif($this->bmi > 25)
            return 1;
        return 0;
    }
    
    public function getDailyKcalAttribute()
    {
        if ($this->gender == 0) // woman
            return (665.151 + (9.567 * $this->weight) + (1.85 * $this->height) - (4.68 * $this->age)) * 1.75;
        else // man
            return (66.47 + (13.7 * $this->weight) + (5.0 * $this->height) - (6.76 * $this->age)) * 1.75;
        
        return 0;
    }
    
    public static function genders() {
        return ['Kobieta', 'Mężczyzna'];
    }
}
