<?php

namespace App\Observers;

use App\User;

class UserObserver
{
    /**
     * Listen to the resource created event.
     *
     * @param  \App\User  $item
     * @return void
     */
    public function creating(User $item)
    {
    }
    
    /**
     *
     * @param  \App\User $item
     * @return void
     */
    public function updating(User $item)
    {
    }
    
    /**
     *
     * @param  \App\User $item
     * @return void
     */
    public function updated(User $item)
    {
    }
    
    /**
     *
     * @param  \App\User $item
     * @return void
     */
    public function saving(User $item)
    {
    }

    /**
     *
     * @param  \App\Client  $item
     * @return void
     */
    public function deleting(User $item)
    {
        // to do removing uploaded avatars
    }
}