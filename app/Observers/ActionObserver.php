<?php

namespace App\Observers;

use App\Action;

class ActionObserver
{
    /**
     * Listen to the resource created event.
     *
     * @param  \App\Action  $item
     * @return void
     */
    public function creating(Action $item)
    {
    }
    
    /**
     *
     * @param  \App\Action $item
     * @return void
     */
    public function updating(Action $item)
    {
    }
    
    /**
     *
     * @param  \App\Action $item
     * @return void
     */
    public function updated(Action $item)
    {
    }
    
    /**
     *
     * @param  \App\Action $item
     * @return void
     */
    public function saving(Action $item)
    {
    }

    /**
     *
     * @param  \App\Action  $item
     * @return void
     */
    public function deleting(Action $item)
    {
        foreach($item->photos as $photo)
            $photo->delete();
    }
}