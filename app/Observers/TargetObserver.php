<?php

namespace App\Observers;

use App\Target;

class TargetObserver
{
    /**
     * Listen to the resource created event.
     *
     * @param  \App\Target  $item
     * @return void
     */
    public function creating(Target $item)
    {
    }
    
    /**
     *
     * @param  \App\Target $item
     * @return void
     */
    public function updating(Target $item)
    {
    }
    
    /**
     *
     * @param  \App\Target $item
     * @return void
     */
    public function updated(Target $item)
    {
    }
    
    /**
     *
     * @param  \App\Target $item
     * @return void
     */
    public function saving(Target $item)
    {
    }

    /**
     *
     * @param  \App\Client  $item
     * @return void
     */
    public function deleting(Target $item)
    {
    }
}