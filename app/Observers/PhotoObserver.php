<?php

namespace App\Observers;

use App\Photo;

class PhotoObserver
{
    /**
     * Listen to the resource created event.
     *
     * @param  \App\Photo  $item
     * @return void
     */
    public function creating(Photo $item)
    {
    }
    
    /**
     *
     * @param  \App\Photo $item
     * @return void
     */
    public function updating(Photo $item)
    {
    }
    
    /**
     *
     * @param  \App\Photo $item
     * @return void
     */
    public function updated(Photo $item)
    {
    }
    
    /**
     *
     * @param  \App\Photo $item
     * @return void
     */
    public function saving(Photo $item)
    {
    }

    /**
     *
     * @param  \App\Photo  $item
     * @return void
     */
    public function deleting(Photo $item)
    {
        \Storage::disk('public')->delete('photos/'.$item->filename);
    }
}