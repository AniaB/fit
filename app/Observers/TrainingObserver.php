<?php

namespace App\Observers;

use App\Training;
use App\Algorithm;

class TrainingObserver
{
    /**
     * Listen to the resource created event.
     *
     * @param  \App\Training  $item
     * @return void
     */
    public function creating(Training $item)
    {
    }
    
    /**
     *
     * @param  \App\Training $item
     * @return void
     */
    public function updating(Training $item)
    {
        $item->kcal = Algorithm::trainingKcal($item->sport, $item->duration);
    }
    
    /**
     *
     * @param  \App\Training $item
     * @return void
     */
    public function updated(Training $item)
    {
    }
    
    /**
     *
     * @param  \App\Training $item
     * @return void
     */
    public function saving(Training $item)
    {
        $item->kcal = Algorithm::trainingKcal($item->sport, $item->duration);
    }

    /**
     *
     * @param  \App\Client  $item
     * @return void
     */
    public function deleting(Training $item)
    {
    }
}