<?php

namespace App;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

use Carbon\Carbon;

class Food extends Model
{
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'product_id', 'meal_id', 'amount'
    ];
    
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
    ];
    
    /**
     * Fieldsto be casted for Carbon dates
     *
     * @var array
     */
    protected $dates = [
        'created_at', 'updated_at',
    ];
    
    public function meal() {
        return $this->belongsTo('\App\Meal');
    }
    
    public function product() {
        return $this->belongsTo('\App\Product');
    }
    
    public function getKcalAttribute(){
        return round($this->amount / 100 * $this->product->kcal);
    }
    
    public function getKcalTextAttribute(){
        return $this->kcal . ' kcal';
    }
    
}
