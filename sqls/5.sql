CREATE TABLE IF NOT EXISTS `actions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `user_id` int(11) NOT NULL,
  `content` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;



ALTER TABLE `targets` ADD `start_weight` DOUBLE NOT NULL DEFAULT '0' AFTER `weight`;
ALTER TABLE `targets` ADD `daily_burnout` DOUBLE NOT NULL DEFAULT '0' AFTER `difference`;
ALTER TABLE `targets` ADD `activated_at` DATETIME NULL DEFAULT NULL AFTER `updated_at`;
ALTER TABLE `users` DROP `average_daily_burnout`;
ALTER TABLE `targets` ADD `finished` TINYINT NOT NULL DEFAULT '0' ;