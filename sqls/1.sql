CREATE TABLE IF NOT EXISTS `trainings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `user_id` int(11) NOT NULL,
  `sport_id` int(11) NOT NULL,
  `duration` int(11) NOT NULL,
  `description` text NOT NULL,
  `kcal` double NOT NULL,
  `editable` tinyint(4) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Struktura tabeli dla tabeli `sports`
--

CREATE TABLE `sports` (
  `id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `name` varchar(255) NOT NULL,
  `factor` double NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `sports`
--

INSERT INTO `sports` (`id`, `created_at`, `updated_at`, `name`, `factor`) VALUES
(1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'Bieganie', 1),
(2, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'Jazda rowerem', 1),
(3, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'Brzuszki', 1);

--
-- Indeksy dla zrzutów tabel
--

--
-- Indexes for table `sports`
--
ALTER TABLE `sports`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT dla tabeli `sports`
--
ALTER TABLE `sports`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;


ALTER TABLE `users` ADD `wunit` VARCHAR(2) NOT NULL DEFAULT 'kg' AFTER `gender`;
ALTER TABLE `users` ADD `hunit` VARCHAR(2) NOT NULL DEFAULT 'm' AFTER `wunit`;

ALTER TABLE `targets` ADD `user_id` INT NOT NULL DEFAULT '1' AFTER `updated_at`;
ALTER TABLE `trainings` ADD `editable` TINYINT NOT NULL DEFAULT '1' AFTER `kcal`;