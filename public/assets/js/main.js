$(document).ready(function () {
    
    $(function () {
        $('#datepicker').datetimepicker({
            format: 'YYYY-MM-DD'
        });
    });
    
    $('.table-sortable tbody').sortable({
        handle: 'span'
    }).bind('sortupdate', function(e, ui) {
        var ids = [];
        
        $('.table-sortable tbody tr').each(function(){
            ids.push($(this).data('id'));
        });
        
        var data = {
            order: ids,
            _token: $('[name=_token]').val()
        };
        
        $.post('/profile/targets/reorder', data, function(){
            console.log('zaktualizowana kolejność');
        });
    });
    
    $('#create-meal button[type=submit]').click(function(e){
        e.preventDefault();
        $div = $(this).closest('#create-meal');
        var $productdata = $div.find('[name=productdata]');
        var $amountdata = $div.find('[name=amountdata]');
        var productname = $productdata.find('option:selected').text();
        var product = $productdata.val();
        var amount =  parseInt($amountdata.val());
        
        if(product !== '' && amount)
        {
            $productdata.val('');
            $amountdata.val('');
            
            var $clone = $div.find('table.hidden tr').clone();
            $clone.find('[data-product] input').val(product);
            $clone.find('[data-product] span').text(productname);
            $clone.find('[data-amount] input').val(amount);
            $clone.find('[data-amount] span').text(amount);
//            console.log([product, amount]);
            $clone.removeClass('hidden').removeAttr('id').appendTo($('#results tbody'));
        }
    });
    
    $(document).on('click', '.remove-row', function (e) {
        e.preventDefault();
        $(this).closest('tr').remove();
    });
    
    $(document).on('click', '.toggle-bottom-row', function (e) {
        e.preventDefault();
        $(this).closest('tr').next('tr').toggleClass('hidden');
        $(this).find('span').toggleClass('glyphicon-chevron-down');
    });
});